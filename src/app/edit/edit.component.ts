import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CoinService } from '../coin.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  coin: any;
  angForm: FormGroup;
  title = 'Edit Coin';
  constructor(private route: ActivatedRoute, private router: Router, private service: CoinService, private fb: FormBuilder) {
    this.createForm();
   }

  createForm() {
    this.angForm = this.fb.group({
      userfirstname: ['', Validators.required ],
      userlastname: ['', Validators.required ],
      useremail: ['', Validators.required ],
      userphone: ['', Validators.required ]
   });
  }

  updateCoin(userfirstname,userlastname,useremail,userphone) {
    this.route.params.subscribe(params => {
    this.service.updateCoin(userfirstname,userlastname,useremail,userphone,params['id']);
    this.router.navigate(['index']);
  });
}
deleteCoin(user_id) {
  this.service.deleteCoin(user_id).subscribe(res => {
    console.log('Deleted');
  });
}


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.coin = this.service.editCoin(params['id']).subscribe(res => {
        this.coin = res;
      });
    });
  }
}